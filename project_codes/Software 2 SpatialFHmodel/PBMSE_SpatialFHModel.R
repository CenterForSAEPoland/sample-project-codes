###############################################################################
###
### This function gives a parametric bootstrap MSE estimator of the EB estimator
### under a Spatial FH model, in which random effects follow a
### Simultaneously Autorregressive (SAR) process.
### The EB estimator is obtained either by REML or by FH fitting methods
###
### Work for European project SAMPLE
###
### Author: Isabel Molina Peralta
### File name: PBMSE_SpatialFHModel.R
### Updated: March 15th, 2010
###
###############################################################################

source("Fitting_SpatialFHModel.R")

PBMSE.SpatialFHmodel<-function(X,Dvec,beta,A,rho,W,n.boot,method="REML",seed=Sys.time()){

  # Set the seed for random number generation, required for the bootstrap method.

  set.seed(seed)
  
  m<-dim(X)[1]  # Sample size or number of areas
  p<-dim(X)[2]  # Num. of auxiliary variables (including intercept)

  # Initial estimators of model coefficients, variance and spatial correlation
  # which will act as true values in the bootstrap procedure.
  
  Bstim.boot<-beta
  rho.boot<-rho
  sigma2.boot<-A

  I<-diag(1,m)
  Xt<-t(X)
  Wt<-t(W)
  
  # Analytical estimators of g1 and g2, used for the bias-corrected PB MSE estimator
  
  g1sp<-rep(0,m)
  g2sp<-rep(0,m)
  
  Amat.sblup<-solve((I-rho.boot*Wt)%*%(I-rho.boot*W))
  G.sblup<-sigma2.boot*Amat.sblup
  V.sblup<-G.sblup+I*Dvec
  V.sblupi<-solve(V.sblup)
  XtV.sblupi<-Xt%*%V.sblupi
  Q.sblup<-solve(XtV.sblupi%*%X)
  
  # Calculate g1
  
  Ga.sblup<-G.sblup-G.sblup%*%V.sblupi%*%G.sblup
  for (i in 1:m) {g1sp[i]<-Ga.sblup[i,i]}

  # Calculate g2
  
  Gb.sblup<-G.sblup%*%t(XtV.sblupi)
  Xa.sblup<-matrix(0,1,p)
  
  for (i in 1:m){
    Xa.sblup[1,]<-X[i,]-Gb.sblup[i,]
    g2sp[i]<-Xa.sblup%*%Q.sblup%*%t(Xa.sblup)
  }

  # Initialize vectors adding g1, g2, g3 and naive PB MSE estimators for each bootstrap replication
  summse.pb<-rep(0,m)
  sumg1sp.pb<-rep(0,m)
  sumg2sp.pb<-rep(0,m)
  sumg3sp.pb<-rep(0,m)
  g1sp.aux<-rep(0,m)
  g2sp.aux<-rep(0,m)

  # Bootstrap cycle starts
  
  for (boot in 1:n.boot) {

    cat("Bootstrap iteration",boot,"\n")

    # Generate a bootstrap sample
    u.boot<-rnorm(m,0,sqrt(sigma2.boot))
    v.boot<-solve(I-rho.boot*W)%*%u.boot
    theta.boot<-X%*%Bstim.boot+v.boot
    e.boot<-rnorm(m,0,sqrt(Dvec))
    direct.boot<-theta.boot+e.boot

    # Fit of the model to bootstrap data

    resultsSp<-fitSpatialFH(X,direct.boot,Dvec,W,method="REML")

    # While the estimators are not satisfactory we generate a new sample
    conv<-resultsSp$convergence
    v<-resultsSp$variance
    r<-resultsSp$spatialcorr
    print(conv)
    print(c(v,r))
    
    if (conv==FALSE) {v<-0}
    if (is.na(v)==TRUE) {v<-0}
    if (is.na(r)==TRUE) {r<-1}

    while ((v<0.0001)|(r<(-0.999))|(r>0.999)){
      print("New sample")
      u.boot<-rnorm(m,0,sqrt(sigma2.boot))
      v.boot<-solve(I-rho.boot*W)%*%u.boot
      theta.boot<-X%*%Bstim.boot+v.boot
      e.boot<-rnorm(m,0,sqrt(Dvec))
      direct.boot<-theta.boot+e.boot
      resultsSp<-fitSpatialFH(X,direct.boot,Dvec,W,method="REML")
      conv<-resultsSp$convergence
      v<-resultsSp$variance
      r<-resultsSp$spatialcorr
      if (conv==FALSE) {v<-0}
      if (is.na(v)==TRUE) {v<-0}
      if (is.na(r)==TRUE) {r<-1}
    }

    # Fit of the model to bootstrap data

    sigma2.simula.ML<-resultsSp$variance
    rho.simula.ML<-resultsSp$spatialcorr
    beta.ML<-resultsSp$modelcoefficients$beta

    # Calculation of the bootstrap Spatial EBLUP

    Amat<-solve((I-rho.simula.ML*Wt)%*%(I-rho.simula.ML*W))
    G<-sigma2.simula.ML*Amat
    V<-G+I*Dvec
    Vi<-solve(V)
    Xbeta<-X%*%beta.ML
    thetaEBLUPSpat.boot<-Xbeta+G%*%Vi%*%(direct.boot-Xbeta)

    # Naive parametric bootstrap MSE

    summse.pb<-summse.pb+(thetaEBLUPSpat.boot-theta.boot)^2

    # Bias-corrected parametric bootstrap:
    # For de bias of g1 and g2, calculate g1sp and g2sp for each bootstrap sample

    XtVi<-Xt%*%Vi
    Q<-solve(XtVi%*%X)

    Ga<-G-G%*%Vi%*%G
    for (i in 1:m) {g1sp.aux[i]<-Ga[i,i]} # g1sp contains the diagonal elements of Ga

    Gb<-G%*%Vi%*%X
    Xa<-matrix(0,1,p)

    for (i in 1:m){
      Xa[1,]<-X[i,]-Gb[i,]
      g2sp.aux[i]<-Xa%*%Q%*%t(Xa)
    }

    # Bootstrap spatial BLUP
    Bstim.sblup<-solve(XtV.sblupi%*%X)%*%XtV.sblupi%*%direct.boot
    Xbeta.sblup<-X%*%Bstim.sblup
    thetaEBLUPSpat.sblup.boot<-Xbeta.sblup+G.sblup%*%V.sblupi%*%(direct.boot-Xbeta.sblup)

    # Parametric bootstrap estimator of g3
    sumg3sp.pb<-sumg3sp.pb+(thetaEBLUPSpat.boot-thetaEBLUPSpat.sblup.boot)^2

    # Expectation of estimated g1 and g2
    sumg1sp.pb<-sumg1sp.pb+g1sp.aux
    sumg2sp.pb<-sumg2sp.pb+g2sp.aux

  } # End of bootstrap cycle

  # Final naive parametric bootstrap MSE estimator
  mse.pb<-summse.pb/n.boot
  # Final bias-corrected bootstrap MSE estimator
  g1sp.pb<-sumg1sp.pb/n.boot
  g2sp.pb<-sumg2sp.pb/n.boot
  g3sp.pb<-sumg3sp.pb/n.boot
  mse.pb2<-2*(g1sp+g2sp)-g1sp.pb-g2sp.pb+g3sp.pb

  # Return naive and bias-corrected parametric bootstrap
  return(data.frame(PBmse=mse.pb,bcPBmse=mse.pb2))

}