#########################################################################
###
### This function fits a unit level model to the log of the welfare 
### variable and the clog-log transformation of the score variable 
### and obtains FAST-EB estimates of domain fuzzy poverty measures
### when the out-of-sample values of auxiliary variables are available.
###
### Work for European project SAMPLE
###
### Author: Caterina Ferretti
### File name: FUZZYpovertyFAST_EB.R
### Updated: February 18th, 2011
###
#########################################################################

FUZZYpovertyFAST.EB<-function(welfare,score,Xs,dom,weight,
z=0.6*median(welfare),alpha=2,L=50){

  # Load library required to fit a unit level model
  
  library(nlme)
  
  # Vector of unique domains
  
  undom<-unique(dom)
  
  # Number of domains

  D<-length(undom)

  # Number of explanatory variables (including intercept)

  p<-dim(Xs)[2]
  
  # Domain sample and population sizes
  
  nd<-rep(0,D)
  Nd<-rep(0,D)

  for (d in 1:D) {
    nd[d]<-sum(dom==d)
    Nd[d]<-sum(round(weight[dom==d]))
    }
    
  # Sample size and estimated population size.
  
  n<-sum(nd)
  N<-sum(Nd)  
    
  # Head Count Ratio, FM and FS indicators,
  # latent and manifest poverty indexes 

  HCRs<-matrix(0,nr=D,nc=1)
  means<-matrix(0,nr=D,nc=1)
  meanss<-matrix(0,nr=D,nc=1)
  lats<-matrix(0,nr=D,nc=1)
  mans<-matrix(0,nr=D,nc=1)
  
  y1s<-NULL
  v1s<-NULL
  y2s<-NULL
  v2s<-NULL
  fms<-NULL
  y1ss<-NULL
  v1ss<-NULL
  y2ss<-NULL
  v2ss<-NULL
  fss<-NULL
  lat<-NULL
  man<-NULL
  sumnd<-0

  for (i in 1:n){
        flags_i<- welfare > welfare[i]
        y1s[i]=sum(weight*flags_i)
        v1s[i]<-y1s[i]/sum(weight)
        y2s[i]=sum(weight*welfare*flags_i)
        v2s[i]<-y2s[i]/sum(weight*welfare)
        fms[i]<-v1s[i]^(alpha-1)*v2s[i]
        flagss_i<- score > score[i]
        y1ss[i]=sum(weight*flagss_i)
        v1ss[i]<-y1ss[i]/sum(weight)
        y2ss[i]=sum(weight*score*flagss_i)
        v2ss[i]<-y2ss[i]/sum(weight*score)
        fss[i]<-v1ss[i]^(alpha-1)*v2ss[i]
        lat[i]<-max(fms[i],fss[i])
        man[i]<-min(fms[i],fss[i])
        }

  for (d in 1:D){ # For each domain d

       Esd<-welfare[dom==d]
       weightd<-weight[dom==d]
              
       HCRs[d,1]<-sum(weightd*(Esd<z))/sum(weightd)

       fmsd<-fms[dom==d]
       fssd<-fss[dom==d]
       latd<-lat[dom==d]
       mand<-man[dom==d]
              
       means[d,1]<-sum(weightd*fmsd)/sum(weightd)
       meanss[d,1]<-sum(weightd*fssd)/sum(weightd)
       lats[d,1]<-sum(weightd*latd)/sum(weightd)
       mans[d,1]<-sum(weightd*mand)/sum(weightd)

        sumnd<-sumnd+nd[d]
      }        

  # Log transformation of the welfare variable "welfare"

  ys<-log(welfare)
  
  # clog-log transformation of the non monetary variable "score"
  
  yss<-log(-log(1-score))

  # Fitting the nested-error model to sample data using R function lme 
  # from library nlme for the welfare variable
  
  fit<-lme(ys~-1+Xs,random=~1|as.factor(dom),method="REML")
  
  # Create a list object containing different results from the model fit
  
  Resultsfit<-list(Summary=summary(fit),FixedEffects=fixed.effects(fit),
  RandomEffects=random.effects(fit),ResVar=fit$sigma,
  RandomEffVar=as.numeric(VarCorr(fit)[1,1]),Loglike=fit$logLik,
  RawResiduals=fit$residuals[1:n])
  
  betaest<-fixed.effects(fit)  # Estimated model coefficients
  upred<-random.effects(fit)   # Predicted random domain effects
  sigmae2est<-fit$sigma^2      # Residual variance
  sigmau2est<-as.numeric(VarCorr(fit)[1,1]) # Cov. matrix of random effects

  # Fitting the nested-error model to sample data using R function lme 
  # from library nlme for the non-monetary variable

  fits<-lme(yss~-1+Xs,random=~1|as.factor(dom),method="REML")
  
  # Create a list object containing different results from the model fits
  
  Resultsfits<-list(Summary=summary(fits),FixedEffects=fixed.effects(fits),
  RandomEffects=random.effects(fits),ResVar=fits$sigma,
  RandomEffVar=as.numeric(VarCorr(fits)[1,1]),Loglike=fit$logLik,
  RawResiduals=fits$residuals[1:n])
                                                                        
  betaests<-fixed.effects(fits) # Estimated model coefficients
  upreds<-random.effects(fits)  # Predicted random domain effects
  sigmae2ests<-fits$sigma^2     # Residual variance
  sigmau2ests<-as.numeric(VarCorr(fits)[1,1]) # Cov. matrix of random ef.

############################################################
# Generation of non-sample values and calculation of
# FASTEB and ELL province poverty fuzzy measures
############################################################

  # Initialize vectors

  # Matrix with domain poverty incidences for each Monte Carlo replication 
  # in fast EB method
  povinc<-matrix(0,nr=D,nc=1)  
  # Vector with estimated domain poverty incidences 
  # by fast EB method
  povincfin<-rep(0,D)
  # Matrix with domain FM indicators for each Monte Carlo replication 
  # in fast EB method          
  povFM<-matrix(0,nr=D,nc=1) 
  # Vector with estimated domain FM indicators by fast EB method  
  povFMfin<-rep(0,D)    
  # Matrix with domain FS indicators for each Monte Carlo replication 
  # in fast EB method       
  povFS<-matrix(0,nr=D,nc=1) 
  # Vector with estimated domain FM indicators by fast EB method  
  povFSfin<-rep(0,D)  
  # Matrix with domain latent poverty for each Monte Carlo replication 
  # in fast EB method         
  LAT<-matrix(0,nr=D,nc=1)
  # Vector with estimated domain latent poverty by fast EB method     
  LATfin<-rep(0,D)      
  # Matrix with domain manifest poverty for each Monte Carlo replication 
  # in fast EB method       
  MAN<-matrix(0,nr=D,nc=1)
  # Vector with estimated domain manifest poverty by fast EB method     
  MANfin<-rep(0,D)             

    for (ell in 1:L){   ### Start of Monte Carlo generations

    samp<-NULL
    Enew<-NULL
    scnew<-NULL
    sumnd<-0 
    
    for (d in 1:D){ # For each domain d

    # Drawing of a sample with the same size of the original sample 
    # and probability proportional to sample weights
    
    weightd<-round(weight[dom==d])
    samp<-c(samp,sample((sumnd+1):(sumnd+nd[d]),size=nd[d],
    replace=TRUE,prob=weightd)) 
    Xr<-Xs[samp,]   
    Xrd<-Xr[(sumnd+1):(sumnd+nd[d]),]
    
    # Compute conditional means for out-of-sample units
           
    mudpred<-Xrd%*%matrix(betaest,nr=p,nc=1)+upred[d,1]

    gammad<-sigmau2est/(sigmau2est+sigmae2est/nd[d])
    sigmav2<-sigmau2est*(1-gammad)

    # Generate random effect for target domain d
    vd<-rnorm(1,0,sqrt(sigmav2))
    
    # Generate random errors for all out-of-sample units in target
    # domain d
    ed<-rnorm(nd[d],0,sqrt(sigmae2est))

    # Compute vector of out-if-sample responses
    ydnew<-mudpred+vd+ed
    
    # Welfare variable, inverse tranformation of response
    
    Ednew<-exp(ydnew)
    Enew<-c(Enew,Ednew)
        
    Xrs<-Xs[samp,]
    
    Xrds<-Xrs[(sumnd+1):(sumnd+nd[d]),]
           
    mudpreds<-Xrds%*%matrix(betaests,nr=p,nc=1)+upreds[d,1]

    gammads<-sigmau2ests/(sigmau2ests+sigmae2ests/nd[d])
    sigmav2s<-sigmau2ests*(1-gammads)

    vds<-rnorm(1,0,sqrt(sigmav2s))
    eds<-rnorm(nd[d],0,sqrt(sigmae2ests))

    ydnews<-mudpreds+vds+eds
    
    # Non-monetary variable, inverse tranformation of response  
    
    scdnew<-1-exp(-exp(ydnews))
    scnew<-c(scnew,scdnew)
        
    sumnd<-sumnd+nd[d]
        }
        
    # FASTEB predictors of poverty measures
        
    y1new<-NULL
    v1new<-NULL
    y2new<-NULL
    v2new<-NULL                    
    fmnew<-NULL
        
    y1news<-NULL
    v1news<-NULL
    y2news<-NULL
    v2news<-NULL                    
    fsnew<-NULL
    
    latnew<-NULL
    mannew<-NULL

    for (i in 1:n){
        
    flagnew_i<- Enew > Enew[i]
    y1new[i]=sum(flagnew_i)
    v1new[i]<-y1new[i]/(n-1)
    y2new[i]=sum(Enew*flagnew_i)
    v2new[i]<-y2new[i]/sum(Enew)

    fmnew[i]<-v1new[i]^(alpha-1)*v2new[i]
        
    flagnews_i<- scnew > scnew[i]
    y1news[i]=sum(flagnews_i)
    v1news[i]<-y1news[i]/(n-1)
    y2news[i]=sum(scnew*flagnews_i)
    v2news[i]<-y2news[i]/sum(scnew)

    fsnew[i]<-v1news[i]^(alpha-1)*v2news[i]
        
    latnew[i]<-max(fmnew[i],fsnew[i])
    mannew[i]<-min(fmnew[i],fsnew[i])
    }

    sumnd<-0

    for (d in 1:D){
        
        Ednew<-Enew[(sumnd+1):(sumnd+nd[d])]
        fmdnew<-fmnew[(sumnd+1):(sumnd+nd[d])]
        fsdnew<-fsnew[(sumnd+1):(sumnd+nd[d])]
        latdnew<-latnew[(sumnd+1):(sumnd+nd[d])]
        mandnew<-mannew[(sumnd+1):(sumnd+nd[d])]

        # FASTEB predictors of poverty measures for each domain
        
        povinc[d,1]<-povinc[d,1]+mean(Ednew<z)
        povFM[d,1]<-povFM[d,1]+mean(fmdnew)
        povFS[d,1]<-povFS[d,1]+mean(fsdnew)
        LAT[d,1]<-LAT[d,1]+mean(latdnew)
        MAN[d,1]<-MAN[d,1]+mean(mandnew)
        
        sumnd<-sumnd+nd[d]

        }  # End of cycle for d
    }  # End of generations


    # FASTEB predictors of poverty measures (averages over the 
    # L Monte Carlo simulations)

    povincfin<-povinc/L
    povFMfin<-povFM/L
    povFSfin<-povFS/L
    LATfin<-LAT/L
    MANfin<-MAN/L

    #Results
    
    EstimatedPoverty<-data.frame(Domain=undom,PopnSize=Nd,SampSize=nd,
    FASTEBPovinc=povincfin,FASTEBFM=povFMfin,FASTEBFS=povFSfin)
    return(list(EstimatedPoverty=EstimatedPoverty,Resultsfit=Resultsfit,
    Resultsfits=Resultsfits))
    
} # end of function FUZZYpovertyFAST.EB

