###############################################################################
###
###          Area level Patitioned F-H model with independent time effects
###                             Pagliarella model 1
###
###
### AUTOR: Maria Chiara Pagliarella
### File name: EstimationMSE.R
### Updated: Dicember 2009
###
### WORKING PROGRESS
###############################################################################

mse.area <- function(X, D, Da, Db, md, sigma2edt, sigmaua, sigmaub, F11, F22) {

    p <- ncol(X)
    i <- list(1:md[1])
    mdcum <- cumsum(md)
    Db <- D-Da
    
    for(d in 2:D){
        i[[d]] <- (mdcum[d-1]+1):mdcum[d]
        if (d<= Da) ia <- i[1:Da]
        else ib <- i[(Da+1):D]
    }
    

    Xda <- list()
    for(d in 1:Da) {
        Xda[[d]] <- X[ia[[d]],]
    }
    Xdb <- list()
    for(d in 1:Db) {
        Xdb[[d]] <- X[ib[[d]],]
    }
    Xd <- list()
    for(d in 1:D) {
        Xd[[d]] <- X[i[[d]],]
    }
    
    Vd.inv <- list()
    Q.inv <- matrix(0, nrow=p, ncol=p)
    for(d in 1:D) {
        ### Elements of the variance matrix
        if (d <= Da)
            vd<-(sigmaua + sigma2edt)[ia[[d]]]
        else
            vd<-(sigmaub + sigma2edt)[i[[d]]]
                    
        ### Inverse matrix of the variance and submatrices
        Vd.inv[[d]] <- diag(1/vd)
        
        ### Inverse of Q. Next we calculate Q
        Q.inv <- Q.inv + t(Xd[[d]]) %*% Vd.inv[[d]] %*% Xd[[d]]

    }
    Q <- solve(Q.inv)
    
    ##############################
    ### Calculation of MSE_A   ###
    ##############################
    
    Vda.inv <- Seda.inv <- SinvXda <- VinvSinvXda <- g2.1a <- list()
    for(d in 1:Da) {
        ### Elements of the variance matrix
        vda<- (sigmaua + sigma2edt)[ia[[d]]]
        
        ### Inverse matrix of the variance and submatrices
        Vda.inv[[d]] <- diag(1/vda)
        
        ### Elements of the variance matrix Sigma_ed
        sda <- sigma2edt[ia[[d]]]
        
        ### Inverse matrix of Sigma_ed for all d submatrices
        Seda.inv[[d]] <- diag(1/sda)
        
        ### Product between Sigma_a^-1_ed and X_da for all d submatrices
        SinvXda[[d]] <- Seda.inv[[d]]%*%Xda[[d]]
        
        ### Product between V^-1_da, Sigma^-1_ed and X_da for all d submatrices
        VinvSinvXda[[d]] <- Vda.inv[[d]]%*%SinvXda[[d]]
        
        ### First part of g2_a (the second is its transpose)
        g2.1a[[d]] <- Xda[[d]] - sigmaua*SinvXda[[d]] + (sigmaua^2) * VinvSinvXda[[d]]
    }
    
    g2a <- mse.a <- list()
    for (d in 1:Da){
        vda <- (sigmaua + sigma2edt)[ia[[d]]]
        q11 <- sigmaua^2 / vda^3
        
        ### Calculation of g_a
        g1a <- (sigmaua * sigma2edt[ia[[d]]]) / vda
        
        g2a[[d]] <- diag(g2.1a[[d]] %*% Q %*% t(g2.1a[[d]]))
        g3a <- q11/F11
        
        g2a[[d]] <- diag(g2.1a[[d]] %*% Q %*% t(g2.1a[[d]]))
        
        mse.a[[d]] <- g1a + g2a[[d]] + 2 * g3a
    }
    
    mse.a <- unlist(mse.a)
    
    
    ##############################
    ### Calculation of MSE_B   ###
    ##############################
    
    Vdb.inv <- Sedb.inv <- SinvXdb <- VinvSinvXdb <- g2.1b <- list()
    for(d in 1:Db) {
        ### Elements of the variance matrix
        vdb<- (sigmaub + sigma2edt)[ib[[d]]]
        
        ### Inverse matrix of the variance and submatrices
        Vdb.inv[[d]] <- diag(1/vdb)
        
        ### Elements of the variance matrix Sigma_ed
        sdb <- sigma2edt[ib[[d]]]
        
        ### Inverse matrix of Sigma_ed for all d submatrices
        Sedb.inv[[d]] <- diag(1/sdb)
        
        ### Product between Sigma_b^-1_ed and X_db for all d submatrices
        SinvXdb[[d]] <- Sedb.inv[[d]]%*%Xdb[[d]]
        
        ### Product between V^-1_db, Sigma^-1_ed and X_db for all d submatrices
        VinvSinvXdb[[d]] <- Vdb.inv[[d]]%*%SinvXdb[[d]]
        
        ### First part of g2_b (the second is its transpose)
        g2.1b[[d]] <- Xdb[[d]] - sigmaub*SinvXdb[[d]] + sigmaub^2*VinvSinvXdb[[d]]
    }
    
    
    g2b <- mse.b <- list()
    for (d in 1:Db){
        vdb <- (sigmaub + sigma2edt)[ib[[d]]]
        q22 <- sigmaub^2/vdb^3
        
        ### Calculation of g_b
        g1b <- (sigmaub * sigma2edt[ib[[d]]]) / vdb
        
        g2b[[d]] <- diag(g2.1b[[d]] %*% Q %*% t(g2.1b[[d]]))
        g3b <- q22/F22
        
        g2b[[d]] <- diag(g2.1b[[d]] %*% Q %*% t(g2.1b[[d]]))
        
        mse.b[[d]] <- g1b + g2b[[d]] + 2 * g3b
    }
    
    mse.b <- unlist(mse.b)

    mse <- c(mse.a, mse.b)

    return(mse)
}
