###############################################################################
###
###          Area level Partitioned F-H model with correlated time effects
###                             Pagliarella model 3
###
###
### AUTOR: Maria Chiara Pagliarella
### File name: EstimationBETA2corr.R
### Updated: May 2010
###
###
###############################################################################


BETA.U.area.2corr <- function(X, ydt, D, Da, Db, md, mda, sigma2edt, sigmaua, sigmaub, rhoa, rhob) {
    
    sigmaua <- sigmaua.hat
    sigmaub <- sigmaub.hat
    rhoa <- rhoa.hat
    rhob <- rhob.hat
        
    p <- ncol(X)
    i <- list(1:md[1])
    mdcum <- cumsum(md)
    Db <- D-Da
    
    for(d in 2:D)
        i[[d]] <- (mdcum[d-1]+1):mdcum[d]
        
    ia <- i[1:Da]
    ib <- i[(Da+1):D]
    
    yd <- Xd <- list()
    for(d in 1:D) {
        yd[[d]] <- ydt[i[[d]]]
        Xd[[d]] <- X[i[[d]],]
    }
    
    
    Vd.inv <- list()
    Q.inv <- matrix(0, nrow=p, ncol=p)
    XVy <-0
    
    for (d in 1:D) {
        ## Elements of the variance matrix TOTAL (A & B)
        if (d <= Da) {
            Omega.a <- matrix(0,nrow=mda[d],ncol=mda[d])
            Omega.a[lower.tri(Omega.a)] <- rhoa^sequence((mda[d]-1):1)
            Omega.a <- Omega.a + t(Omega.a)
            diag(Omega.a) <- 1
            Omega.a <- (1/(1-rhoa^2)) * Omega.a
            vd <-(sigmaua * Omega.a + diag(sigma2edt[ia[[d]]]))}
        else {
            Omega.b <- matrix(0,nrow=md[d],ncol=md[d])
            Omega.b[lower.tri(Omega.b)] <- rhob^sequence((md[d]-1):1)
            Omega.b <- Omega.b + t(Omega.b)
            diag(Omega.b) <- 1
            Omega.b <- (1/(1-rhob^2)) * Omega.b
            vd <-(sigmaub * Omega.b + diag(sigma2edt[i[[d]]]))}
        
        ### Inverse matrix of the variance and submatrices
        Vd.inv[[d]] <- solve(vd)
        
        ### Product between X^t_d,  V^-1_d and y_d for all d submatrices
        XVy <- XVy + t(Xd[[d]]) %*% Vd.inv[[d]] %*% yd[[d]]
        
        ### Inverse of Q. Next we calculate Q
        Q.inv <- Q.inv + t(Xd[[d]]) %*% Vd.inv[[d]] %*% Xd[[d]]
    }
    # print(Omega.a)
    # print(Omega.b)
    # print(vd)
        
    Q <- solve(Q.inv)
    
    beta <- Q %*% XVy
    
    
    ua <- ub <- list()
    for (d in 1:D){
        if (d <= Da) {
            ua[[d]] <- sigmaua * Omega.a %*% Vd.inv[[d]] %*% (yd[[d]] - Xd[[d]] %*% beta)}
        else {
            ub[[d]] <- sigmaub * Omega.b %*% Vd.inv[[d]] %*% (yd[[d]] - Xd[[d]] %*% beta)}
    }
        
    ua<-as.matrix(unlist(ua))
    ub<-as.matrix(unlist(ub))
    u <- c(ua,ub)
    
    return(c(beta,u))
}
