###############################################################################
###############################################################################
###
###                    C�lculo de G1,G2 yG3 para modelo de area                
###                              Proyecto SAMPLE                               
###                              Proyecto SAMPLE                               
###                              01 Septiembre 2009                            
###

### AUTOR: Laureano Santamaria Arana

### Editado por:
### con fecha:
### cambios realizados:         

### Comentarios: 


G1 <- function(X, Y, W, D, md, ndi, sigma0, sigma1, sigma2, NDI) { 
    
    n <- nrow(X)
    F1 <- sigma1/sigma0  
    F2 <- sigma2/sigma0
 
    # ndi muestral
    # NDI poblacional 
    
    mdcum <- cumsum(md)
    ndcum <- cumsum(ndi)

    fdi <- ndi/NDI
    
    for(d in 1:D) {      
        if (d==1) {
            Inicio <-1
            Pr <-1
            F <- md[d]
        }
        if (d!=1) {
            Pr <- (ndcum[mdcum[d-1]]+1)
            Inicio <-mdcum[d-1]+1
            F <- Inicio + md[d] - 1
        }
        Fin  <- ndcum[mdcum[d]]
        Nd <- Fin-Pr+1
        
        Wd <- W[Pr:Fin,Pr:Fin]
        yd <- Y[Pr:Fin]
        Xd <- X[Pr:Fin,]
        
        fd <- fdi[Inicio:F]
                   
        D1Nd <- matrix(0,Nd,md[d])
        i <- 1
        for(k in 1:md[d]) {
            for(j in 1:ndi[Inicio+k-1]) {
                D1Nd[i,k]<-1 
                i<- i+1
            }
        } 
        Imd<-diag(md[d])      
        UnoNd <-as.matrix(rep(1,Nd))

        sld.inv <- solve(Imd+F2*t(D1Nd)%*%Wd%*%D1Nd)
        Ld.inv <- Wd-F2*Wd%*%D1Nd%*%sld.inv%*%t(D1Nd)%*%Wd
        T1 <- F1*Ld.inv%*%UnoNd%*%t(UnoNd)%*%Ld.inv
        T2 <- 1+(F1*t(UnoNd)%*%Ld.inv%*%UnoNd)       
        Sigmad.inv <- Ld.inv - (T1/T2[1,1])  
        
        for(k in 1:md[d]) {
            M1 <- Imd[k,]*(1-fd[k])
            P1d <- sigma0*F1*((1-fd[k])^2)*(1-(F1*t(UnoNd)%*%Sigmad.inv%*%UnoNd)) 
            P2d <- -sigma0*F1*F2*(1-fd[k])*(t(UnoNd)%*%Sigmad.inv%*%D1Nd%*%M1) 
            P3d <- (sigma0*F2*((1-fd[k])^2)) - (sigma0*F2*F2*(diag(t(M1)%*%t(D1Nd)%*%Sigmad.inv%*%D1Nd%*%M1)))
            if (k==1) {
                g1d <- P1d+P2d+P3d
            }
            if (k!=1) {
                g1d <- c(g1d,P1d+P2d+P3d) 
            }   
        }
        if (d==1) {
            g1 <- g1d
        }
        if (d!=1) {
            g1 <- c(g1,g1d) 
        }        
    }
    return(g1)   
}

G2 <- function(X, Y, W, D, md, ndi, sigma0, sigma1, sigma2, NDI, MXp, MXm) { 
    
    n <- nrow(X)
    p <- ncol(X)
    F1 <- sigma1/sigma0  
    F2 <- sigma2/sigma0
 
    # ndi muestral
    # NDI poblacional
    # MXp Media poblacional
    # MXm Media muestral
     
    M <- sum(md)

    mdcum <- cumsum(md)
    ndcum <- cumsum(ndi)

    fdi <- ndi/NDI
    D1 <- NDI-ndi
    K1 <- NDI/D1
    K2 <- ndi/D1
    Xast <- K1*MXp - K2*MXm
    
    mR <- 0
    
    for(d in 1:D) {      
        if (d==1) {
            Inicio <-1
            Pr <-1
            F <- md[d]
        }
        if (d!=1) {
            Pr <- (ndcum[mdcum[d-1]]+1)
            Inicio <-mdcum[d-1]+1
            F <- Inicio + md[d] - 1
        }
        Fin  <- ndcum[mdcum[d]]
        Nd <- Fin-Pr+1        
        
        Wd <- W[Pr:Fin,Pr:Fin]
        yd <- Y[Pr:Fin]
        Xd <- X[Pr:Fin,]
        fd <- fdi[Inicio:F]
        Xdast <- Xast[Inicio:F]
                   
        D1Nd <- matrix(0,Nd,md[d])
        i <- 1
        for(k in 1:md[d]) {
            for(j in 1:ndi[Inicio+k-1]) {
                D1Nd[i,k]<-1 
                i<- i+1
            }
        } 
        Imd<-diag(md[d])      
        UnoNd <-as.matrix(rep(1,Nd))
        
        sld.inv <- solve(Imd+F2*t(D1Nd)%*%Wd%*%D1Nd)
        Ld.inv <- Wd-F2*Wd%*%D1Nd%*%sld.inv%*%t(D1Nd)%*%Wd
        T1 <- F1*Ld.inv%*%UnoNd%*%t(UnoNd)%*%Ld.inv
        T2 <- 1+(F1*t(UnoNd)%*%Ld.inv%*%UnoNd)       
        Sigmad.inv <- Ld.inv - (T1/T2[1,1]) 
        
        mR <- mR + t(Xd)%*%Sigmad.inv%*%Xd              
    }
    Q <- sigma0*solve(mR)   
    
    for(d in 1:D) {      
        if (d==1) {
            Inicio <-1
            Pr <-1
            F <- md[d]
        }
        if (d!=1) {
            Pr <- (ndcum[mdcum[d-1]]+1)
            Inicio <-mdcum[d-1]+1
            F <- Inicio + md[d] - 1
        }
        Fin  <- ndcum[mdcum[d]]
        Nd <- Fin-Pr+1   
        
        D1Nd <- matrix(0,Nd,md[d])
        i <- 1
        for(k in 1:md[d]) {
            for(j in 1:ndi[Inicio+k-1]) {
                D1Nd[i,k]<-1 
                i<- i+1
            }
        } 
        Imd<-diag(md[d])      
        UnoNd <-as.matrix(rep(1,Nd))

        Wd <- W[Pr:Fin,Pr:Fin]
        Xd <- X[Pr:Fin,]
        fd <- fdi[Inicio:F]
        Xdast <- Xast[Inicio:F]

        sld.inv <- solve(Imd+F2*t(D1Nd)%*%Wd%*%D1Nd)
        Ld.inv <- Wd-F2*Wd%*%D1Nd%*%sld.inv%*%t(D1Nd)%*%Wd
        T1 <- F1*Ld.inv%*%UnoNd%*%t(UnoNd)%*%Ld.inv
        T2 <- 1+(F1*t(UnoNd)%*%Ld.inv%*%UnoNd)       
        Sigmad.inv <- Ld.inv - (T1/T2[1,1]) 
             
        for(k in 1:md[d]) {
            G11d <- (F1*(1-fd[k]))*(1-(F1*t(UnoNd)%*%Sigmad.inv%*%UnoNd))%*%t(UnoNd)%*%Wd%*%Xd
            G12d <- -F1*F2*(1-fd[k])*(t(UnoNd)%*%Sigmad.inv%*%D1Nd%*%t(D1Nd)%*%Wd%*%Xd)
            G21d <- -F1*F2*(1-fd[k])*(t(Imd[k,])%*%t(D1Nd)%*%Sigmad.inv%*%UnoNd%*%t(UnoNd)%*%Wd%*%Xd)
            T1   <- Imd - (F2*t(D1Nd)%*%Sigmad.inv%*%D1Nd) 
            G22d <- F2*(1-fd[k])*t(Imd[k,])%*%T1%*%t(D1Nd)%*%Wd%*%Xd
            
            if (k==1) {
                G2d <- G11d + G12d + G21d + G22d
            }
            if (k!=1) {
                G2d <- c(G2d, G11d + G12d + G21d + G22d) 
            }        
        }
        if (d==1) {
            G2 <- G2d
        }
        if (d!=1) {
            G2 <- c(G2, G2d) 
        }        
        
    }

    at22 <- matrix(G2,nrow=M,ncol=p,byrow = T)
    at21 <- matrix((1-fdi)*Xast,nrow=M,ncol=p,byrow = T) 
       
    g2 <- diag((at21-at22)%*%Q%*%(t(at21)-t(at22)))

    return(g2)
 
}

G3 <- function(X, Y, W, D, md, ndi, sigma0, sigma1, sigma2, NDI, Finv) { 
    
    n <- nrow(X)
    F1 <- sigma1/sigma0  
    F2 <- sigma2/sigma0
 
    # ndi muestral
    # NDI poblacional  
    
    mdcum <- cumsum(md)
    ndcum <- cumsum(ndi)
    fdi <- ndi/NDI
    
    mR <- 0
    
    q<-matrix(0, nrow=3, ncol=3)      
    
    for(d in 1:D) {      
        if (d==1) {
            Inicio <-1
            Pr <-1
            F <- md[d]
        }
        if (d!=1) {
            Pr <- (ndcum[mdcum[d-1]]+1)
            Inicio <-mdcum[d-1]+1
            F <- Inicio + md[d] - 1
        }
        Fin  <- ndcum[mdcum[d]]
        Nd <- Fin-Pr+1        
        
        Wd <- W[Pr:Fin,Pr:Fin]
        yd <- Y[Pr:Fin]
        Xd <- X[Pr:Fin,]
        fd <- fdi[Inicio:F]
                   
        D1Nd <- matrix(0,Nd,md[d])
        i <- 1
        for(k in 1:md[d]) {
            for(j in 1:ndi[Inicio+k-1]) {
                D1Nd[i,k]<-1 
                i<- i+1
            }
        } 
        Imd<-diag(md[d])      
        UnoNd <-as.matrix(rep(1,Nd))
        
        Ads.inv <- solve(Imd+(F2*t(D1Nd)%*%Wd%*%D1Nd))
        Lds.inv <- Wd-(F2*Wd%*%D1Nd%*%Ads.inv%*%t(D1Nd)%*%Wd)
       
        T1 <- F1*Lds.inv%*%UnoNd%*%t(UnoNd)%*%Lds.inv
        T2 <- 1+(F1*t(UnoNd)%*%Lds.inv%*%UnoNd)
        Sigmad.inv <- Lds.inv - (T1/T2[1,1])  

        T1 <- -Wd%*%D1Nd%*%Ads.inv%*%t(D1Nd)%*%Wd
        T2 <- F2*Wd%*%D1Nd%*%Ads.inv%*%t(D1Nd)%*%Wd%*%D1Nd%*%Ads.inv%*%t(D1Nd)%*%Wd       
        DpLdsF2 <- T1 + T2
        
        T1 <- Lds.inv%*%UnoNd%*%t(UnoNd)%*%Lds.inv
        T2 <- 1+(F1*t(UnoNd)%*%Lds.inv%*%UnoNd)
        DpSdsF1 <- (T1/(T2[1,1]^2))
        
        T0 <- as.numeric((F1*F1*t(UnoNd)%*%DpLdsF2%*%UnoNd))
        T1 <- (T0*(Lds.inv%*%UnoNd%*%t(UnoNd)%*%Lds.inv))/(T2[1,1]^2)
        T3 <- (DpLdsF2%*%UnoNd%*%t(UnoNd)%*%Lds.inv) + (Lds.inv%*%UnoNd%*%t(UnoNd)%*%DpLdsF2)       
        DpSdsF2 <- DpLdsF2 + T1 - (F1*T3)/(T2[1,1])
                
        Sigmads <- solve(Sigmad.inv)
        for(k in 1:md[d]) {
            DpB1dF1 <- (1-fd[k])*t(UnoNd)%*%(Sigmad.inv + F1*DpSdsF1)                
            DpB1dF2 <- F1*(1-fd[k])*t(UnoNd)%*%DpSdsF2
        
            DpB2dF1 <- F2*(1-fd[k])*Imd[k,]%*%t(D1Nd)%*%DpSdsF1
            DpB2dF2 <- (1-fd[k])*Imd[k,]%*%t(D1Nd)%*%(Sigmad.inv + F2*DpSdsF2) 
        
            T2 <- (DpB1dF1+DpB2dF1)
            T3 <- (DpB1dF2+DpB2dF2)


            q[1,1] <- 0
            q[1,2] <- 0
            q[1,3] <- 0
            q[2,1] <- 0        
            q[2,2] <- T2%*%(sigma0*Sigmads)%*%t(T2)
            q[2,3] <- T2%*%(sigma0*Sigmads)%*%t(T3)        
            q[3,1] <- 0               
            q[3,2] <- T3%*%(sigma0*Sigmads)%*%t(T2)
            q[3,3] <- T3%*%(sigma0*Sigmads)%*%t(T3)
        
            g3dparcial <- sum(diag(q%*%Finv))
            if (k==1) {
                g3d <- g3dparcial
             }
            if (k!=1) {
                g3d <- c(g3d,g3dparcial) 
            }        
        }
        
        if (d==1) {
            g3 <- g3d
        }
        if (d!=1) {
            g3 <- c(g3,g3d) 
        }        

    }
    
    return(g3)

}


G4 <- function(D, md, ndi, NDI, W, sigma0) { 

 
    # ndi muestral
    # NDI poblacional
     
    g4 <- vector()

    Indice <- 1
    Inicio <- 1
    for(d in 1:D) {   
        for (j in 1:md[d]) {
            g4[Indice] <- (sigma0/(NDI[Indice]^2))*(NDI[Indice]-ndi[Indice])
            Indice <- Indice + 1
        }
    }

    return(g4)
}




mse.area <- function(X, Y, W, D, md, ndi, MXm ,NDI, MXp, sigma0, sigma1, sigma2, FInv) {
   
    g1 <- G1(X, Y, W, D, md, ndi, sigma0, sigma1, sigma2, NDI)
    g2 <- G2(X, Y, W, D, md, ndi, sigma0, sigma1, sigma2, NDI, MXp, MXm)
    g3 <- G3(X, Y, W, D, md, ndi, sigma0, sigma1, sigma2, NDI, FInv)
    g4 <- G4(D, md, ndi, NDI, W, sigma0)
           
    return(g1+g2+2*g3+g4)
}
